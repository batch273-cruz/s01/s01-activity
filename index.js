/*
	QUIZ
1. cosnt arr = ["item1", "item2"]
2. .at(0)
3. .at(-1)
4. indexOf()
5. forEach()
6. map()
7. every()
8. some()
9. False
10. True

*/

// Activity Function Coding

// 1. Add to End

students = ['John', 'Joe', 'Jane', 'Jessie'];

function addToEnd(students, newStudent) {

	if (typeof newStudent === 'string'){
		students.push(newStudent);
		return students;
	} else {
		return ("error - can only add strings to an array");
	}
};

// 2. Add to Start

function addToStart(students, newStudent) {

	if (typeof newStudent === 'string'){
		students.unshift(newStudent);
		return students;
	} else {
		return ("error - can only add strings to an array");
	}
};

// 3. Element Checker

function elementChecker(students, name) {
	if (students.indexOf("Jane") !== -1) {
		return true
	} else {
		return ("error - passed in array is empty");

	}
}

// 4. Check all strings ending

function checkAllStringsEnding(students, e) {
	if (students.length === 0 || students === undefined) {
		return ("error - array must Not be empty")
	} else if (typeof students.length !== 'string') {
		return ("error - all array elements must be strings")
	} 
}


// 5. Sort

function sortStudents (students) {
	students.sort(function(a,b) {return a.length - b.length});
	return students
}

